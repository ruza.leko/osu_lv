lstNum=[]
suma=0
while True:
    num=input('Unos: ')
    if(num=='Done'):
        break
    if(num.isdigit()):
        lstNum.append(int(num))
        #suma+=int(num)
    else:
        print('Uneseni znak nije u zadanom obliku')
        continue

print(f'Uneseno je : {len(lstNum)} znaka')
print('Srednja vrijednost: ',sum(lstNum)/len(lstNum))
print('Min vrijednost: ', min(lstNum))
print('Max vrijednost: ', max(lstNum))
lstNum.sort()
print('Sortirana lista:',lstNum)
